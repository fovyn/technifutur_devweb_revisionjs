const Personne = (function(firstName, lastName, birthDate = null) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
});

Personne.prototype.sayHello = function() { console.log(this.firstName, this.lastName, this.birthDate); }

const p = new Personne("Flavian", "Ovyn");

p.sayHello()

class Personne2 {
    #firstName;
    #lastName;
    #birthDate;

    constructor(firstName, lastName, birthDate = null) {
        this.#firstName = firstName;
        this.#lastName = lastName;
        this.#birthDate = birthDate;
    }

    get FirstName() { return this.#firstName; }
    get LastName() { return this.#lastName; }
    get BirthDate() { return this.#birthDate; }

    sayHello() {
        console.log(this.#firstName, this.#lastName, this.#birthDate);
    }
}

const p2 = new Personne2("Flavian", "Ovyn");
console.log(p2.FirstName, p2.LastName, p2.BirthDate);
p2.sayHello()
