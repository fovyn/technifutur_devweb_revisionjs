const m = moment().add(1, "day");
const date = new Date(); //Equivalent DateTime en C#

console.log(date);
console.log(date.getDate()); //Le jour du calendrier
console.log(date.getDay()); // Le jours de la semaine (0= Dimanche, 1= Lundi, ...)
console.log(date.getFullYear()); //L'année
